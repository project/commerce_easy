<?php

namespace Drupal\commerce_easy\Event;

final class EasyEvents {
  const EASY_BUILD_ORDER = 'commerce_easy.build_order';
}
